import { IEncryptedMessage } from "@kiltprotocol/types";

import { ICredentialPresentation } from "@kiltprotocol/sdk-js";
import {
  IEncryptedMessageV1,
  PubSubSessionV1,
  PubSubSessionV2,
} from "../../types";

export async function tryToLogIn(
  extensionSession: PubSubSessionV1 | PubSubSessionV2 | null
): Promise<ICredentialPresentation> {
  if (!extensionSession) {
    throw new Error(
      "No Extension Session Object found. Start the Server-Extension-Session first! "
    );
  }

  const getRequestResponse = await fetch(`/api/credential/request`, {
    method: "GET",
    credentials: "include",
    headers: {
      accessControlAllowOrigin: "*",
      "Content-type": "application/json",
      Accept: "application/json",
    },
  });
  if (!getRequestResponse.ok) {
    console.log(getRequestResponse);
    throw Error(getRequestResponse.statusText);
  }

  const encryptedCredentialRequest = await getRequestResponse.json();
  console.log(
    `encryptedCredentialRequest gotten from the server: ${JSON.stringify(
      encryptedCredentialRequest,
      null,
      2
    )}`
  );

  // prepare to receive the credential from the extension

  // initialize so that typescript don't cry
  let extensionMessage: IEncryptedMessageV1 | IEncryptedMessage = {
    receiverKeyId: "did:kilt:4someones#publicKeyAgreement",
    senderKeyId: "did:kilt:4YourDecentralizedApp#publicKeyAgreement",
    ciphertext: "string",
    nonce: "string",
  };

  console.log("extension: ", extensionSession);

  await extensionSession.listen(async (message) => {
    extensionMessage = message;
  });

  // Now we can pass the message to the extension.
  // Meaning, we can request the Credential.

  await extensionSession.send(encryptedCredentialRequest);
  // Now the extension should ask the user to give a Credential fitting the requested cType.

  // Send the Credential to the Backend to be verified
  const responseToBackend = JSON.stringify(extensionMessage);

  const credentialVerificationResponse = await fetch("/api/credential/submit", {
    method: "POST",
    credentials: "include",
    headers: {
      "Content-type": "application/json",
      Accept: "application/json",
    },
    body: responseToBackend,
  })
    .then((res) => res.json())
    .catch(
      (err) => new Error("Login Failed. Error verifying the Credential.", err)
    );

  console.log(credentialVerificationResponse);
  return credentialVerificationResponse;
}
