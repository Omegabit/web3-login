import {
  PubSubSessionV1,
  PubSubSessionV2,
} from "kilt-extension-api/dist/types/types";

export async function startExtensionSession(
  extension: any
): Promise<PubSubSessionV1 | PubSubSessionV2 | undefined> {
  // generate a JSON-Web-Token with session values on the backend and save it on a Cookie on the Browser:
  const serverSessionStart = await fetch(`/api/session/start`, {
    method: "GET",
    credentials: "include",
    headers: {
      accessControlAllowOrigin: "*",
      "Content-type": "application/json",
      Accept: "application/json",
    },
  });
  if (!serverSessionStart.ok) {
    throw Error(serverSessionStart.statusText);
  }

  // At the start, we only want the Server Session Values (and they are only ones available)
  const startSessionPayload = (await serverSessionStart.json()).server;

  if (!startSessionPayload) {
    throw new Error("Trouble generating session values on the backend.");
  }

  console.log(
    'Plain text accompanying the Cookie "sessionJWT": ',
    startSessionPayload
  );

  try {
    // destructure the payload:
    const { dAppName, dAppEncryptionKeyUri, challenge } = startSessionPayload;

    // Let the extension do the counterpart:
    const extensionSession = await extension.startSession(
      dAppName,
      dAppEncryptionKeyUri,
      challenge
    );
    console.log("the session was initialized (¬‿¬)");
    console.log("session being returned by the extension:", extensionSession);

    // Resolve the `session.encryptionKeyUri` and use this key and the nonce
    // to decrypt `session.encryptedChallenge` and confirm that it’s equal to the original challenge.
    // This verification must happen on the server-side.

    const responseToBackend = JSON.stringify({ extensionSession });

    const sessionVerificationResponse = await fetch(`/api/session/verify`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-type": "application/json",
        Accept: "application/json",
      },
      body: responseToBackend,
    });

    if (!sessionVerificationResponse.ok) {
      console.log("Session could not be verified.");
      return;
    }

    console.log(
      "Session successfully verified. dApp-Server and Extension trust each other."
    );
    return extensionSession;
  } catch (error) {
    // tryToLogin ...
    console.error(
      `Error verifying Session from:  ${extension.name}: ${extension.version},  ${error}`
    );

    // return startSessionPayload;
  }
}
