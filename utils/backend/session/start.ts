import * as Kilt from "@kiltprotocol/sdk-js";
import { CookieSerializeOptions, serialize } from "cookie";
import type { NextApiRequest, NextApiResponse } from "next";

import { JWTPayload, SignJWT } from "jose";
import { DAPP_NAME, JWT_SIGNER_SECRET } from "../config";
import { getApi } from "../connection";
import { fetchDidDocument } from "../fetchDidDocument";

/**
 * Define how the Session Values are packaged.
 *
 * At the start, we only have the values from the server.
 * After verification, we also save the values that the extension (wallet) send us.
 */
export interface SessionValues {
  server: {
    dAppName: string;
    dAppEncryptionKeyUri: Kilt.DidResourceUri;
    challenge: string;
  };
  extension?: {
    encryptedChallenge: string;
    encryptionKeyUri: Kilt.DidResourceUri;
    nonce: string;
  };
}

// Set Cookie Options: (list of ingredients)
export const cookieOptions: CookieSerializeOptions = {
  // Indicates the number of seconds until the Cookie expires.
  maxAge: 60 * 60 * 24,
  // only send over HTTPS
  secure: true,
  // prevent cross-site request forgery attacks
  sameSite: "strict",
  // restricts URL that can request the Cookie from the browser. '/' works for the entire domain.
  path: "/",
  // Forbids JavaScript from accessing the cookie
  httpOnly: true,
};

export async function generateSessionValues(
  didDocument: Kilt.DidDocument
): Promise<JWTPayload> {
  // connects to the websocket of your, in '.env', specified blockchain
  await getApi();

  // Build the EncryptionKeyUri so that the client can encrypt messages for us:
  const dAppEncryptionKeyUri =
    `${didDocument.uri}${didDocument.keyAgreement?.[0].id}` as Kilt.DidResourceUri;

  if (typeof didDocument.keyAgreement === undefined) {
    throw new Error("This DID has no Key Agreement. Cannot encrypt like this.");
  }

  // Generate a challenge to ensure all messages we receive are fresh.
  // A UUID is a universally unique identifier, a 128-bit label. Here expressed as a string of a hexadecimal number.
  // It is encourage that you personalize your challenge generation.
  const challenge = Kilt.Utils.UUID.generate();

  const sessionValues = {
    server: {
      dAppName: DAPP_NAME,
      dAppEncryptionKeyUri: dAppEncryptionKeyUri,
      challenge: challenge,
    },
  };

  console.log("Session Values just generated", sessionValues);

  return sessionValues;
}

/**
 * Saving the session values as a JSON-Web-Token on a Cookie of the browser
 */
export async function startSession(
  request: NextApiRequest,
  response: NextApiResponse
): Promise<void> {
  const DidDocument = await fetchDidDocument();

  // we use the DID-Document from the dApp fetched on server-start to generate our Session Values:
  const payload = await generateSessionValues(DidDocument);
  const secretKey = JWT_SIGNER_SECRET;
  if (!secretKey) {
    throw new Error(
      "Define a value for 'JWT_SIGNER_SECRET' on the '.env'-file first!"
    );
  }

  // Create a Json-Web-Token:
  const JWTsecretKey = Buffer.from(secretKey);

  const token = await new SignJWT(payload) // details to  encode in the token
    .setProtectedHeader({ alg: "HS256" }) // algorithm
    .setIssuedAt()
    .setExpirationTime(`${cookieOptions.maxAge} seconds`) // set the expiration of JWT same as the Cookie
    .sign(JWTsecretKey); // secretKey generated from previous step

  console.log("start JWT Token", token);

  // Set a Cookie in the header including the JWT and our options:
  // Using 'cookie-parser' dependency:
  const cookie = serialize("sessionJWT", token, cookieOptions);

  response.setHeader("Set-Cookie", cookie);

  // send the Payload as plain text on the response, this facilitates the start of the extension session.
  response.status(200).send(payload);
}
