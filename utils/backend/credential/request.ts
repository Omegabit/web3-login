import * as Kilt from "@kiltprotocol/sdk-js";
import { randomAsHex } from "@polkadot/util-crypto";

import { encryptionCallback } from "../encryptionCallback";
import { generateKeypairs } from "../generateKeyPairs";
import { readSessionCookie } from "../readSessionCookie";

import { SessionValues } from "../session/start";
import { NextApiRequest, NextApiResponse } from "next";
import { DAPP_DID_MNEMONIC, DAPP_DID_URI, JWT_SIGNER_SECRET } from "../config";
import { getErrorMessage } from "../../handleError";

// Polimec TODO: Here we should change:
// - the cTypeHash to the one of the Credential Issuer
// - the trustedAttesters to the one of the Credential Issuer
// - the requiredProperties that the user needs to disclose
const emailRequest: Kilt.IRequestCredentialContent = {
  cTypes: [
    {
      cTypeHash:
        "0x3291bb126e33b4862d421bfaa1d2f272e6cdfc4f96658988fbcffea8914bd9ac",
      trustedAttesters: [
        "did:kilt:5CqJa4Ct7oMeMESzehTiN9fwYdGLd7tqeirRMpGDh2XxYYyx" as Kilt.DidUri,
      ],
      requiredProperties: ["Email"],
    },
  ],
};

export async function getRequestCredential(
  request: NextApiRequest,
  response: NextApiResponse,
) {
  try {
    // read cookie from browser

    const sessionValues: SessionValues = await readSessionCookie(
      request,
      response,
      JWT_SIGNER_SECRET,
    );

    if (!sessionValues.extension) {
      throw new Error(
        "Extension Session Values not found. Try restarting and verifying the server-extension-session.",
      );
    }

    // We need the encryptionKeyUri from the Extension
    const { did: claimerSessionDidUri } = Kilt.Did.parse(
      sessionValues.extension.encryptionKeyUri,
    );

    const message = requestWrapper(emailRequest, claimerSessionDidUri);

    console.log(
      "the message with the Credential-Request before encryption: ",
      JSON.stringify(message, null, 2),
    );

    const encryptedMessage = await encryptMessage(message, sessionValues);

    return response.send(encryptedMessage);
  } catch (error: any) {
    let error_message = getErrorMessage(error);
    return response.status(500).send(error_message);
  }
}

/** Turns the Credential Request into a Kilt.Message.
 *  It also generates and includes a challenge on that message.
 */
function requestWrapper(
  credentialRequest: Omit<Kilt.IRequestCredentialContent, "challenge">,
  receiverDidUri: Kilt.DidUri,
): Kilt.IMessage {
  const challenge = randomAsHex();
  const messageBody: Kilt.IRequestCredential = {
    content: { ...credentialRequest, challenge: challenge },
    type: "request-credential",
  };

  const message = Kilt.Message.fromBody(
    messageBody,
    DAPP_DID_URI,
    receiverDidUri,
  );
  return message;
}

/**
 * Protects from undesired readers.
 */
async function encryptMessage(
  message: Kilt.IMessage,
  sessionObject: SessionValues,
): Promise<Kilt.IEncryptedMessage> {
  const { keyAgreement: ourKeyAgreementKeyPair } = generateKeypairs(
    DAPP_DID_MNEMONIC,
  );

  if (!sessionObject.extension) {
    throw new Error(
      "Receivers Encryption Key needed in order to encrypt a message",
    );
  }

  const ourKeyIdentifier = sessionObject.server.dAppEncryptionKeyUri;
  const theirKeyIdentifier = sessionObject.extension.encryptionKeyUri;

  const cypheredMessage = await Kilt.Message.encrypt(
    message,
    encryptionCallback({
      keyAgreement: ourKeyAgreementKeyPair,
      keyAgreementUri: ourKeyIdentifier,
    }),
    theirKeyIdentifier,
  );

  return cypheredMessage;
}
