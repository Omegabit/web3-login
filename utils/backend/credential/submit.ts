import * as Kilt from '@kiltprotocol/sdk-js'
import { NextApiRequest, NextApiResponse } from "next";

import { generateKeypairs } from '../generateKeyPairs'
import { decryptionCallback } from '../decryptionCallback'
import { getApi } from '../connection'
import { DAPP_DID_MNEMONIC } from '../config';
import { getErrorMessage } from '../../handleError';

export async function postSubmitCredential(
  request: NextApiRequest,
  response: NextApiResponse
) {
  try {
    const encryptedMessage = request.body
    console.log(
      `encrypted Message that the server obtained ${JSON.stringify(
        encryptedMessage,
        null,
        2
      )}`
    )
    const api = await getApi()

    const { keyAgreement } = generateKeypairs(DAPP_DID_MNEMONIC)
    const decryptedMessage = await Kilt.Message.decrypt(
      encryptedMessage,
      decryptionCallback(keyAgreement)
    )

    // Verifying this is a properly-formatted message
    Kilt.Message.verify(decryptedMessage)

    if (decryptedMessage.body.type !== 'submit-credential') {
      throw new Error(`Unexpected message type: ${decryptedMessage.body.type}`)
    }

    // FIX-ME!:  maybe allow for several credentials in the future
    const credential = decryptedMessage.body.content[0]

    console.log('Decrypted Credential being verify: \n', credential)

    // FIX-ME!: server needs to have challenge and cType that requested from user to make a proper verification
    await Kilt.Credential.verifyPresentation(credential, {
      challenge: credential.claimerSignature.challenge
    })

    const attestationChain = await api.query.attestation.attestations(
      credential.rootHash
    )
    const attestation = Kilt.Attestation.fromChain(
      attestationChain,
      credential.rootHash
    )
    if (attestation.revoked) {
      throw new Error("Credential has been revoked and hence it's not valid.")
    }

    //FIX-ME! (Written by KILT): need to send the email to the frontend

    console.log("Credential Successfully Verified!  The user is legitimate. ( ͡° ͜ʖ ͡°)")

    response
      .status(200)
      .json(
        credential.claim.contents
      )
  } catch (error: any) {
    let error_message = getErrorMessage(error)
    return response.status(500).send(error_message)
  }
}