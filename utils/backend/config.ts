import { DidUri } from "@kiltprotocol/sdk-js";

// Letting the server know where the environment variables are

export const PORT = process.env.PORT || 3000;
export const WSS_ADDRESS = process.env.WSS_ADDRESS || "wss://peregrine.kilt.io";
export const DAPP_DID_MNEMONIC =
  process.env.DAPP_DID_MNEMONIC ||
  "cinnamon system intact quantum drip year web donor noodle ignore firm prepare";
export const DAPP_DID_URI =
  process.env.DAPP_DID_URI as DidUri ||
  ("did:kilt:4scvBvDmps5xzaNEQwe9UorMSvnRVaVPS7ptzAGUiG7Uhi3R" as DidUri);
export const DAPP_NAME = process.env.DAPP_NAME || "Polimec";
export const DAPP_ACCOUNT_MNEMONIC =
  process.env.DAPP_ACCOUNT_MNEMONIC ||
  "cinnamon system intact quantum drip year web donor noodle ignore firm prepare";
export const JWT_SIGNER_SECRET =
  process.env.JWT_SIGNER_SECRET || "secret_key_for_jwt_signing";
