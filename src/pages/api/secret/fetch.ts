import type { NextApiRequest, NextApiResponse } from "next";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // TODO: Check if the req contains the correct secret
  return res.status(200).json({ message: "Hello World" });
}
