// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { postSubmitCredential } from "../../../../utils/backend/credential/submit";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Check if the request is a POST request
  if (req.method !== "POST") {
    res.status(405).json({ error: "Method not allowed, please use POST" });
    return;
  }
  // TODO: Maybe we can check the payload here
  return postSubmitCredential(req, res);
}
