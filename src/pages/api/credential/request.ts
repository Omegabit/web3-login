// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { getRequestCredential } from "../../../../utils/backend/credential/request";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Check if the request is a GET request
  if (req.method !== "GET") {
    res.status(405).json({ error: "Method not allowed, please use GET" });
    return;
  }
  return getRequestCredential(req, res);
}
