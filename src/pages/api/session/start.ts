// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { startSession } from "../../../../utils/backend/session/start";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  return startSession(req, res);
}
