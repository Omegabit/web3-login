// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { verifySession } from "../../../../utils/backend/session/verify";

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  return verifySession(req, res);
}
