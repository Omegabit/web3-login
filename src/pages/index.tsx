import { AuthContext } from "@/context/authContext";
import { Types } from "kilt-extension-api";
import {
  PubSubSessionV1,
  PubSubSessionV2,
} from "kilt-extension-api/dist/types/types";
import { Inter } from "next/font/google";
import { useContext, useState } from "react";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const authContext = useContext(AuthContext);
  const [lib, setLib] = useState<any>(null);
  const [isSession, setIsSession] = useState<boolean>(false);
  const [email, setEmail] = useState<string>("");
  const [extensions, setExtensions] = useState<
    Types.InjectedWindowProvider<
      Types.PubSubSessionV1 | Types.PubSubSessionV2
    >[]
  >([]);

  const [extensionSession, setExtensionSession] = useState<
    PubSubSessionV1 | PubSubSessionV2 | null
  >(null);

  async function login() {
    authContext.login();
  }

  async function testApi() {
    const result = await fetch("/api/secret/fetch");
    const message = await result.json();
    console.log(message);
  }

  // useEffect(() => {
  //   const loadLib = async () => {
  //     const lib = await import("kilt-extension-api");
  //     setLib(lib as any);
  //   }
  //   loadLib();
  // }, [])

  // Directly inject the extensions that support the KILT protocol
  // useEffect(() => {
  //   if (!lib) {
  //     return;
  //   }

  //   const { watchExtensions } = lib;

  //   watchExtensions((extensions: any) => {
  //     setExtensions(extensions);
  //     console.log(extensions);
  //   });
  // }, [lib]);

  return (
    <main
      className={`flex min-h-screen flex-col items-center p-24 ${inter.className}`}
    >
      <Button onClick={testApi}>GO TO SECRET PAGE</Button>

      {/* <Button onClick={() => authContext.startSession()}>
        {" "}
        Start a Session{" "}
      </Button> */}

      <Button disabled={!authContext.extensionSession} onClick={() => login()}>
        Login!
      </Button>

      {email ? <p className="text-2xl">Logged in as {email}</p> : null}
    </main>
  );
}

interface Props {
  [x: string]: any;
}

function Button({ children, ...props }: Props) {
  return <button {...props}>{children}</button>;
}
