import { Types } from "kilt-extension-api";

import { ICredentialPresentation } from "@kiltprotocol/sdk-js";
import {
  PubSubSessionV1,
  PubSubSessionV2,
} from "kilt-extension-api/dist/types/types";
import { FC, createContext, useEffect, useState } from "react";
import { startExtensionSession } from "../../utils/frontend/startExtensionSession";
import { tryToLogIn } from "../../utils/frontend/tryToLogiIn";

interface IAuthContext extends React.HTMLAttributes<"div"> {
  startSession: () => Promise<void>;
  extensionSession: PubSubSessionV1 | PubSubSessionV2 | null;
  isSessionConnected: boolean;
  isLoggedIn: boolean;
  login: () => Promise<void>;
  presentation?: ICredentialPresentation;
}

export const AuthContext = createContext<IAuthContext>({} as IAuthContext);

export const AuthContextProvider: FC<React.HTMLAttributes<"div">> = ({
  children,
}) => {
  const [lib, setLib] = useState<any | undefined>(undefined);
  const [isSessionConnected, setIsSessionConnected] = useState<boolean>(false);
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  const [presentation, setPresentation] = useState<
    ICredentialPresentation | undefined
  >(undefined);

  const [extensions, setExtensions] = useState<
    | Types.InjectedWindowProvider<
        Types.PubSubSessionV1 | Types.PubSubSessionV2
      >[]
    | undefined
  >(undefined);

  const [extensionSession, setExtensionSession] = useState<
    PubSubSessionV1 | PubSubSessionV2 | null
  >(null);

  useEffect(() => {
    const loadLib = async () => {
      const lib = await import("kilt-extension-api");
      setLib(lib);
    };
    loadLib();
  }, []);

  useEffect(() => {
    if (!lib) {
      return;
    }

    const { watchExtensions, initializeKiltExtensionAPI } = lib;

    watchExtensions((extensions: any) => {
      setExtensions(extensions);
    });

    try {
      !extensions && initializeKiltExtensionAPI();
    } catch (e) {
      // this is for development purposes, hot reloads will cause this to throw
    }
  }, [lib, extensions]);

  useEffect(() => {
    if (lib && extensions && extensions.length > 0) {
      // Auto start session
      doStartSession();
    }
  }, [lib, extensions]);

  const doStartSession = async () => {
    if (lib && extensions && extensions.length > 0) {
      console.log(extensions);
      const session = await startExtensionSession(extensions[0]);

      setExtensionSession(session!);
    }
  };

  const login = async () => {
    console.log(
      "Trying to log in. Meaning to ask the extension for a specific Type of Credential - a CType."
    );
    if (extensionSession) {
      const res = await tryToLogIn(extensionSession);
      setPresentation(res);
      console.log("claim:", res.claim);
      // setEmail(res);
    }
  };

  // logger part
  useEffect(() => {
    console.log("credential wallet session changed:", extensionSession);
    setIsSessionConnected(!!extensionSession);
  }, [extensionSession]);

  useEffect(() => {
    console.log("credential presentation:", presentation);
    setIsLoggedIn(!!presentation);
  }, [presentation]);

  return (
    <AuthContext.Provider
      value={{
        startSession: doStartSession,
        extensionSession,
        isSessionConnected,
        isLoggedIn,
        login,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
